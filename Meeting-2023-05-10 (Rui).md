Wednesday, 10 am

https://docs.google.com/document/d/11GylkA-7AozM5EaWIcXWgmg8mvCz4hWUNox5VvHxRxA/edit#heading=h.k4int6ehs0es


Ben joined today here because the other meeting is cancelled.
Rui will leave in 11 for a meeting. 
Rui: released models.
Ben: needed those models out for board meeting

### Office refurnish 
Ben: No including tables, 
other items need to move in boxes, if you have items that you worried about then move by weekend.
Be prepared, may get mail. 

Yui: office leave is going to be longer?
Ben: Looks like. 

### Yui
Briefing on Harrison meeting (Monday)
Working on four models: Phynet, Earthformer, metnet, climax. 

Rui: details pease for Edison and Maruf.
Yui: Using the data are sources from NCI
They are planning a **united Interface** for model and data 
Need help with multi-GPU/node support. 

Earth-net challenge.
EarthNet2021

Rui: question is can we use it our work. what can we learn?
Edison: can not unify them, very difficult.
Edison: if it was possible then Google, and Meta would have done it (???)

Yue: Can we do it for ClimaxNext
Edison: I don't want to do it. 
Yue: So, it is possible but hard. 
Rui: Yes, looks like a lot of work.
Edison: Hugging face have models zoo.  Model it after Hugging face model.

Rui: Harrison created multi-processing without Dask. 
We need to know how he did it and compared with Dask.

Rui: Next step.
Yue: Multi-process vs Dask data loading time compare. 
Rui: all models working?
Yue: Implementation is there. 

Rui: Maruf worked with Met-net. 
Me: Met-net works only on random data, 
if Harrison has a working example then it is fantastic. 
Also, check if Model parallelism on 16 GPUs is done by Harrison.
Yue: Code in the /g/data, will send the link after meeting to check. 
/g/data/kd24/PythonPackages/edit_repos/models/

Ben: put a link about Destination Earth of ANU. 
Yue: Can we have a dataset for contest. 
Edison: lecture about we need to put a specific task. 
Ben: We participate or organising.
Yue: organising 
Ben: I see. 

### Edison:
ERA5 ESM. 
Ben: Eling is silent ?
Edison: Yes. 
Ben: FourCastNext??
Edison: My work is finished, in Rahul's hand now. 
Ben: Your next task, I have some plans with dataset structure. 
Edison: Bussy with ERA5 crawling

### Rahul
working on release IceNet. 
wb00 or Internet data option.

About Fourcastnet had meeting and documentation need to be updated for data dir.
FourCastNext, writing notebook, to compare predictions from two and Ground truth. 
Created graphs for RMSE.
How to create sub dir for FourCastNext and FourCastnet, two data.

Rui: Rahul use Pytorch profiler to compare the both.
Rui has COSIMA meeting, need to leave

### Me
Ben: two types of downscaling ???
I explained, it is same to the ML model. 

Ben: Make Queensland dataset pics the same size:  ground truth and prediction. 
Ben: check if the variables are available  and get back to Ben. 

Ben: Also, use BARRA (Bureau DATA)
Also, use local dataset rather than downloading. 
We will use BARRA  in the future:
https://dx.doi.org/10.4225/41/5993927b50f53

BARRA 2 is being build, but we got not the data yet.
Ben: Tenessne was asking high resolution for different region. 
For example, BARRA has region.
And high resolution predict is done on city and not other area.
Can Deep learning do predictions for other regions 

Edison: Can not be done without base.
Ben: Can we try first on the city areas downscaling, then apply to other areas. 
We have have BERRA, and use DL4DS on Sydney regeoin, make the it work for other regions.

Edison: use CMIP  I will send links.

Ben: NSW has two resolution and have a look. if you can figure out if it can be done on other regions. 

For monday, use CMIP data, do the same thing you did for QLD.
Use the UNSW dir data,  for inference. 
Will be same small presentation. 

CMIP6 is being prepared, and will be used in future for our work. 
BARRA uses ERA5 no

Edison: Local data use. 
Ben: We need to use local satellite data. 

Ben: Look for data before asking,  so without being look silly.





















