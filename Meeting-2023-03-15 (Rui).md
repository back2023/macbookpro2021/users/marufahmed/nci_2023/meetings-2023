Wednesday, 10 am

https://docs.google.com/document/d/11GylkA-7AozM5EaWIcXWgmg8mvCz4hWUNox5VvHxRxA/edit#

Rui : Edison and Rahul did a lot of work on Fourcastnet

### Edison: 
working on generating one year prediction with fourcastnet. 
Rui: send flags/parameters to Rahul for inference. 

### Me:
Rui: where are the scripts/notebooks? scratch fp0
Work with Rahul, show him what you have done.
Next release will be Climax. Rahul will validate
Rui: Put you data in wb00. 

### Yui:
Climax to model parallel ? using Deepspeed?
Rui: sure. 

Talking about data loader and I/O performance. 
Rui: Maruf, parallel dataloader? 
Me: Dali, PyTorch lighting

### Rui:
Working on deepseed.
Cuda version problem. 
Another release of some software. 

### Fourcastnet release, Rahul: 
Rui: Rahul working with data management team to put data on wb00 space. 
Edison will add metadata to dataset. 
Working on my notebook and somethings are not clear to him. 
Rahul want to talk to me about visualisation. 

Rui: use ANU account for globus.
Maruf did you used ANU account?
Me: No
Rui: use ANU globus for more information. 


### Meeting with Rahul
Just explained some figs
