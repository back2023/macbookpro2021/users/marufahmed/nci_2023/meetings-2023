Wednesday, 9 am
https://docs.google.com/document/d/11GylkA-7AozM5EaWIcXWgmg8mvCz4hWUNox5VvHxRxA/edit#

### Edison 
PDF of fourcastnet, will share later. 
Baseline comes from me, 
Edison created another model called, 1%....

Q: how many steps prediction? more than 40 steps
dates?
Edison was reluctant, but Rui asked him to check how far the prediction can go.

### Me 
showed metnet-2 and weatherbench
Ben show it tomorrow to BOM

### Yui
Working with reinforcement learning with alpha-zero.ji
showed an example. 
working with ViT on Imagenet. 
Tensor parallelism ??

Rui: in future Edison and Maruf's work can merge with this work. 

### Rahul
working with fourcastnet data release. 
working on ice-net, but an error. documentation is done
Working with MLOps with GitHub. 
Creating a small demo with scripts. 
Rui: CI/CD working. 
Rahul having problem import PyTorch on Jupyter, Rui is helping him on that. 

### Rui
Released new version of ml env
Pushed another version of deep-speed, singularity, still doing testing.
Harrison requested the deep-speed. 
NCI indexing BARRA

### Ben
ESM index 
NSW, Queensland downscaling data is being added to NCI
Edison will index them.
Last week meet with ACCESS NRI, they are using indices for models. 
Kelsey was happy. 

In documents, VOD to ARE transfer links.
Yui on two weeks leave, back in 26 April. 
Ben: meetings will go on. 
Keep Working on present work, but need to go back to downscaling later.
Also, some work on notebooks may be needed like ice-net notebook. 

### Other comments
Yui: talking about a benchmark, replicate BERT+Resnet, on Saphaire rapids. Code with mlbench in public. 

Ben: Jingbo is focused on training with something else. We are more focused with research. 
**Fred** from training asked about reinforcement learning and Julia. 
Ben: wait and see. We may do some training, on climate and Physics ML.
WE DO NOT WANT BAD TRAINING, so prepare first. 
Ben: training on primary subjects do not produce results, so we wait for better training that can produce long lasting results. 

