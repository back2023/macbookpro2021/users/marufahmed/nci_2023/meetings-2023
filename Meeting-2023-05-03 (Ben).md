Wednesday, 9 am 

https://docs.google.com/document/d/11GylkA-7AozM5EaWIcXWgmg8mvCz4hWUNox5VvHxRxA/edit#heading=h.k4int6ehs0es

### Edison
ESM 
Ben: new data called cordex - cmip6 will be added soon. 
Some file may be re-written. Ben knows about the data. 
Crawling new data and script.
Ben has a PDF document, Edison will have a look at it.

Edison prepared for notebook on FourCastNet

### Rui

Ben: TERN does not won the data is it govt data.
Is TERN dataset is already in Coredx data set or not?
Check it, we already may have it. 
Rui: which project? 
Ben: Rui work with Maruf, to figure out the TERN dataset. 
Rui: showed doc and notebook for DL4DS. also, will be put on the github. 

Ben: need to write a report, and use the links for these projects. 
So, make them ready by the end of the week.

Also, NCI training is showing easy things, people said.
so we need to polish our work and publish those projects. 
They asked about ML to Jinbo, not to Ben.
So, we need to push those projects.

Yui: Date line? 
Ben: by end of this week, that's why need the links. 
Pages have to be in good form. 

### Rahul
working with ice-net. 
will make metadata template by today, 
Also, working with FourCastNext scripts. 
Also, writing thesis. 

### Me: 
Ben: Climax,  are we doing with our data?
Me: Not msissing a parameter. 
Ben: ERA5 is his juristiriction. the parameter can be requested if needed.

Ben: PDE and omniverse are interesting to us. 
Ominverse, interested for long time and talk to who are working in NCI.
PDE is also interesting. 

Rui: link for PDE, work done by Yui
https://opus.nci.org.au/display/DAE/Neural+Network+Augmented+Differential+Equations

Me showed omniverse lisence price
https://www.colfax-intl.com/downloads/nvidia-omniverse-enterprise-packaging-pricing-and-licensing-guide-september-2021.pdf

### Yui:
Working with DeepSpeed. 
Rui: https://arxiv.org/abs/2203.11141

Ben: 
Tennesse asked about Hackthon type event in a two weeks window.
We will talk tomorrow wit BOM.
Rui: based on GADI platform? 
Ben: Both AWS and GADI, we need to prepare for both. 

Yui: 18-19th may? training event or small group?
Ben: We will virtual, 
Attendee are us, tennesse, and Harrison. 
Proof of concept presentation from us. 
Yui will do it.
We will talk tomorrow. 

oceanigan, a ocean model based on Julia, 
based on Princeton model, and developed. 

### Back to Rahul
Rui: 
Edison want to compare Fourcastnet and FourCastNexT, and put it in the notebook?
How to provide those scripts to users? Can run on Notebook?
Edison: Yes.
Rui: implement a short train on notebook, and watch on profiler.
Rui: Can we do the same for original ?
Edison: Not
Rui: If we can compare the both then it will be great. 

Yui: some idea for prediction compare. 

### Stay back and Work with Ben and Rui to find if TERN is already present in NCI.

Path: /g/data/rr3/CORDEX
Ben: doc link https://opus.nci.org.au/display/CMIP/CMIP5-CORDEX+datasets
Details about data set.

Rui: link for Queensland. chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/https://dap.tern.org.au/thredds/fileServer/CMIP5QLD/Queensland_FutureClimate_Data_Availability.pdf

https://dap.tern.org.au/thredds/fileServer/CMIP5QLD/Queensland_FutureClimate_Data_Availability.pdf

Maruf have a look at the NCI dataset, find and if we already have it or not? 

Rui:
 https://dap.tern.org.au/thredds/catalog/CMIP5QLD/CMIP5_Downscaled_CCAM_QLD10/RCP45/monthly/MaximumTemperature/catalog.html

Ben: We do not the dataset, we need to download. 
Also, have a look at `rr3` and see if you can apply DL4DS.

I send it to everyone: 
https://www.longpaddock.qld.gov.au/qld-future-climate/data-info/tern/

Ben: NSW model. they are coming to Canberra in two weeks time. 
/g/data/rr3/publications/CORDEX/output/AUS-44/UNSW
Have a look, close the loop quickly. 
Me: I have to write script
Ben: send a message in teams to Rui and Ben, as soon as you finished 

Next, notebook for  UNSW , AUS-44 will need to be visualised with notebook in high resolution. 

Ben: 
A lot of New data are coming, and we need to apply DL4DS, as soon as soon as possible
So, do something today, as soon as possible.

Ben: Release ?
Rui: we are working
Ben: if we can apply DL4DS on rr3, it will be great. 

Maruf need to work on NSW data, it is more important. 
Maruf have a look the data, and it is more important. 
No need to release the Queensland data yet. 

Meet in this afternoon, before 3:30 or after 4:30
Meet queens land tomorrow and NSW on Friday. 
So, we need to do it today. 


### Afternoon meeting-2:30

Ben: We are copying, Queensland data. 

Ben: Use compare two images, accuracy.
Me: Yes I can use Python module
Rui: try different parameter?
Me: yes, we can

Ben:
Create a new doc page for UNSW,  screen capture, not notebook.
Make the DL4D structure like the fourcastnet.
Do not use cut and paste at the top. 

Ben: Make prediction picture for UNSW. 
Rui: copy paste and create a new page. 
Ben: make the page more like Fourcastnet page. 

Ben: might invite us to Queensland data demo on Monday.  
1:30 t0 2:30 meeting, monday. 

Then, a weeks time, in Friday, or Monday, have normal meeting with NSW. around 1:30.
Might bring me up earlier for demo for top guy.  

Rui: make opus page and script ready and then let Rui know.
Ben: meet again on tomorrow at 1, instead of 4:30
