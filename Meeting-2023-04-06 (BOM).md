Thursday-9:30 am

### Edison
only 1% budget to train the entire fourcastnet.
Several methods used, described in PDF.
Will send the pdf by mail
tennessee.leeuwenburg@bom.gov.au
harrison.cook@bom.gov.au

Tenesse: that is impressive, very interested. 
what about a year predictions? 
Rahul: almost ready. 
Rui: data will be in gdata, script in dk92. 

Rui: We released DeepSpeed, now doing testing. 
Also, working on Julia. 
Yui: The first implementation is Alpha zero. 
A documentation is being prepared. 
Also working with oceananigans, a notebook is being prepared in dk92. 
Ben: Also, Cosima group is looking into it. it uses Julia. 


### Showed my work
metnet-2 and Weather-bench. 
Tenesse: 58% reduction is interesting.

Harrison: more interested in Fourcastnet. 
Interested in implementing the paper, ray cluster. 
Further plan.
Edison: Release source code and pre-train weight. 

Tenesse: BOM code may be released code through NCI, because it is easier. 

Tenesse: Within a few months implement code to input BOM data, but should not take the whole year. 
Harrison: Happy to share his work. 

Rui: NetCDF is working. code shared in chat:
"[rxy900@gadi-login-02 ~]$ module use /g/data/dk92/apps/Modules/modulefiles/
[rxy900@gadi-login-02 ~]$ module load NCI-ai-ml/23.03
[rxy900@gadi-login-02 ~]$ python
Python 3.9.16 (main, Jan 11 2023, 16:05:54)
[GCC 11.2.0] :: Anaconda, Inc. on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import netCDF4
>>> import xarray
>>> import dask
>>> import ray
>>> netCDF4.__version__
'1.6.0'
>>>"

Ben: ACCESS NRI will have a conference in Sep, one breakout will be on ML.

Tenesse: Has two new student projects
One is doing image super resolution
Another, on scoring method for earth machine learning models. 

Yui: Comment about Edison work on fourcastnet results, compare.
Tenesess: RMSE may not be a good statistics.
Harrison: Can provide a verification support for publish. have tools and knowledge to verify. 
Tenesse: Multi objective function is needed. RMSE is not enough. 
Want to provide, a standard for industry to measure the climate models. 
At least write a paper.

Ben: BOM dataset, which one??
Tenesse: "openradar.io"
Ben: "https://dx.doi.org/10.25914/5cb686a8d9450"

